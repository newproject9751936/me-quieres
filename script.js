document.addEventListener('DOMContentLoaded', () => {
    const yesBtn = document.getElementById('yesBtn');
    const noBtn = document.getElementById('noBtn');

    yesBtn.addEventListener('click', () => {
        alert('¡Lo sabía, yo también te quiero!');
    });

    const moveButton = () => {
        const screenWidth = window.innerWidth;
        const screenHeight = window.innerHeight;
        const btnRect = noBtn.getBoundingClientRect();

        // Define the central area (50% of screen width and height centered)
        const centralAreaWidth = screenWidth * 0.5;
        const centralAreaHeight = screenHeight * 0.5;
        const centralAreaLeft = (screenWidth - centralAreaWidth) / 2;
        const centralAreaTop = (screenHeight - centralAreaHeight) / 2;

        let newLeft = centralAreaLeft + Math.random() * (centralAreaWidth - btnRect.width);
        let newTop = centralAreaTop + Math.random() * (centralAreaHeight - btnRect.height);

        // Ensure the button moves away from the current position
        while (Math.abs(newLeft - btnRect.left) < btnRect.width && Math.abs(newTop - btnRect.top) < btnRect.height) {
            newLeft = centralAreaLeft + Math.random() * (centralAreaWidth - btnRect.width);
            newTop = centralAreaTop + Math.random() * (centralAreaHeight - btnRect.height);
        }

        noBtn.style.left = `${newLeft}px`;
        noBtn.style.top = `${newTop}px`;
    };

    noBtn.addEventListener('mouseover', moveButton);
    noBtn.addEventListener('touchstart', moveButton); // For mobile devices
});
